defmodule Eidetic.Projector.Middleware.MongoDbIdempotenceTest do
  use ExUnit.Case, async: true

  alias Eidetic.Projector.Middleware.MongoDbIdempotence

  setup_all do
    {:ok, connection} = Mongo.start_link(database: random_string(12), seeds: ["mongodb:27017"])

    {:ok, %{mongodb: connection}}
  end

  test ~s/it can set the replay property to true and false/, %{mongodb: mongodb} do
    {:ok, pid} =
      GenServer.start_link(
        MongoDbIdempotence,
        [
          mongodb: mongodb,
          collection: "idempotence"
        ],
        name: MongoDbIdempotence
      )

    mock = TestSupport.Mocks.get_bundle()

    # Never seen before, should be false
    {:ok, result} = MongoDbIdempotence.process(:pre, mock)

    assert %{replay: false} = Map.get(result, :modifiers)

    {:ok, _} = MongoDbIdempotence.process(:post, mock)

    # Processed above, should be true
    {:ok, result} = MongoDbIdempotence.process(:pre, mock)

    assert %{replay: true} = Map.get(result, :modifiers)
  end

  def random_string(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64() |> binary_part(0, length)
  end
end
