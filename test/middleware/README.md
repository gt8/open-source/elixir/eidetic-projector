# Idempotence (mongoDB) Middleware for Eidetic Projector

This middleware provides both `:pre` and `:post` processing functionality for Eidetic Projector.

## Preprocess

When added as a preprocess middleware, this will add a new modifier to the bundle. The modifier will tell you if this event has been processed before.

If mongoDB is unavailable, this will inform Eidetic Projector to retry the bundle.

If the event has never been processed by this consumer, then the modifier will reflect: `replay: false`

If the event has been processed by this consumer, then the modifier will reflect `replay: true`

## Postprocess

When added as a postprocess middleware, this will ensure that any processed events are recorded in mongoDB. If the mongoDB write fails, this middleware will block any further consuming until mongoDB is back online.

## Future

1. Batching. Definitely batching / caching of writes until we hit the configured batch size.

2. At the moment, we use mongoDB for idempotence; so we've only included this implementation. In the future, we will extract this out and adopt the adapter pattern; allowing for other implementions.
