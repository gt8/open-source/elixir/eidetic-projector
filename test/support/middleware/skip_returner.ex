defmodule TestSupport.Middleware.SkipReturner do
  alias Eidetic.Event
  alias Eidetic.Projector.Bundle
  alias Eidetic.Projector.Middleware

  @behaviour Middleware

  def process(_, bundle = %Bundle{event: %Event{payload: %{pid: pid}}}) do
    _ = send(pid, __MODULE__)

    {:skip, bundle}
  end
end
