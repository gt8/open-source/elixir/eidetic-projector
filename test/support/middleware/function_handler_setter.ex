defmodule TestSupport.Middleware.FunctionHandlerSetter do
  alias Eidetic.Event
  alias Eidetic.Projector.Bundle
  alias Eidetic.Projector.Middleware

  @behaviour Middleware

  def process(:pre, bundle = %Bundle{}) do
    {:ok,
     Map.put(bundle, :handler, fn inside_bundle = %Bundle{event: %Event{payload: %{pid: pid}}} ->
       send(pid, __MODULE__)

       {:ok, inside_bundle}
     end)}
  end
end
