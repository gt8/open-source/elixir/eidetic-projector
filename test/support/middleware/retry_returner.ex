defmodule TestSupport.Middleware.RetryReturner do
  alias Eidetic.Event
  alias Eidetic.Projector.Bundle
  alias Eidetic.Projector.Middleware

  @behaviour Middleware

  def process(_, %Bundle{event: %Event{payload: %{pid: pid, counter: counter}}}) do
    Agent.update(counter, &(&1 + 1))

    _ = send(pid, __MODULE__)

    :retry
  end
end
