defmodule TestSupport.Middleware.OkReturner do
  alias Eidetic.Event
  alias Eidetic.Projector.Bundle
  alias Eidetic.Projector.Middleware
  require Logger

  @behaviour Middleware

  def process(_, bundle = %Bundle{event: %Event{payload: %{pid: pid}}}) do
    _ = Logger.debug(fn -> inspect(bundle) end)

    _ = send(pid, __MODULE__)

    {:ok, bundle}
  end
end
