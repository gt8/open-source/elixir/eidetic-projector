defmodule TestSupport.Middleware.AtomHandlerSetter do
  alias Eidetic.Event
  alias Eidetic.Projector.Bundle
  alias Eidetic.Projector.Middleware

  @behaviour Middleware

  def process(:pre, bundle = %Bundle{}) do
    {:ok, Map.put(bundle, :handler, __MODULE__)}
  end

  def handle(bundle = %Bundle{event: %{payload: %{pid: pid}}}) do
    send(pid, __MODULE__)

    {:ok, bundle}
  end
end
