defmodule TestSupport.Mocks do
  alias Eidetic.Event
  alias Eidetic.Projector.Bundle
  alias Eidetic.Projector.Bundle.ConsumerMeta

  def get_bundle() do
    %Bundle{
      consumer_meta: %ConsumerMeta{
        topic: "",
        partition: 0,
        offset: 0
      },
      event: %Event{
        type: "MyEventType",
        version: 1,
        identifier: "#{:rand.uniform(50_000)}",
        serial_number: :rand.uniform(12),
        payload: %{}
      },
      handler: nil,
      result: nil
    }
  end
end
