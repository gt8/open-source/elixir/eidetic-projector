defmodule Eidetic.ProjectorTest do
  use ExUnit.Case, async: false
  alias Eidetic.Event
  alias Eidetic.Projector
  alias Eidetic.Projector.Bundle
  alias Eidetic.Projector.Bundle.ConsumerMeta
  alias TestSupport.Middleware.AtomHandlerSetter
  alias TestSupport.Middleware.ErrorReturner
  alias TestSupport.Middleware.FunctionHandlerSetter
  alias TestSupport.Middleware.OkReturner
  alias TestSupport.Middleware.RetryReturner
  alias TestSupport.Middleware.SkipReturner

  test ~s/it can be started with a default name/ do
    {:ok, pid} = Projector.start_link(deserializer: fn payload -> payload end)

    assert Process.whereis(Projector) == pid
  end

  test ~s/it uses the default name when called using helper function/ do
    {:ok, pid} = Projector.start_link(deserializer: fn payload -> %Event{} end)

    assert {:ok, %Bundle{}} = Projector.consume(%ConsumerMeta{}, nil)
  end

  test ~s/it can be started with it's own name/ do
    {:ok, pid} = Projector.start_link(name: :whatever, deserializer: fn payload -> payload end)

    assert Process.whereis(:whatever) == pid
  end

  test ~s/it can be configured with preprocessing middleware/ do
    pidload = %{pid: self()}

    {:ok, pid} =
      Projector.start_link(
        deserializer: fn payload -> %Event{payload: pidload} end,
        preprocess: [
          OkReturner
        ]
      )

    assert {:ok, bundle} = GenServer.call(pid, {:consume, %ConsumerMeta{}, nil})

    assert bundle.event.payload == pidload

    assert_receive OkReturner
  end

  test ~s/it can handle errors returned from middleware/ do
    pidload = %{pid: self()}

    {:ok, pid} =
      Projector.start_link(
        deserializer: fn payload -> %Event{payload: pidload} end,
        preprocess: [
          ErrorReturner,
          OkReturner
        ]
      )

    {:error, bundle} = GenServer.call(pid, {:consume, %ConsumerMeta{}, nil})

    assert bundle.event.payload == pidload

    assert_received ErrorReturner
    refute_received OkReturner
  end

  test ~s/it can handle skip returned from middleware/ do
    pidload = %{pid: self()}

    {:ok, pid} =
      Projector.start_link(
        deserializer: fn payload -> %Event{payload: pidload} end,
        preprocess: [
          OkReturner,
          SkipReturner,
          ErrorReturner
        ]
      )

    {:skip, bundle} = GenServer.call(pid, {:consume, %ConsumerMeta{}, nil})

    assert bundle.event.payload == pidload

    assert_received OkReturner
    assert_received SkipReturner
    refute_received ErrorReturner
  end

  test ~s/it calls the configured function handler when everything is OK/ do
    pidload = %{pid: self()}

    {:ok, pid} =
      Projector.start_link(
        deserializer: fn payload -> %Event{payload: pidload} end,
        preprocess: [
          FunctionHandlerSetter
        ]
      )

    assert {:ok, bundle} = GenServer.call(pid, {:consume, %ConsumerMeta{}, nil})

    assert_received FunctionHandlerSetter
  end

  test ~s/it calls the configured atom handler when everything is OK/ do
    pidload = %{pid: self()}

    {:ok, pid} =
      Projector.start_link(
        deserializer: fn payload -> %Event{payload: pidload} end,
        preprocess: [
          AtomHandlerSetter
        ]
      )

    assert {:ok, bundle} = GenServer.call(pid, {:consume, %ConsumerMeta{}, nil})

    assert_received AtomHandlerSetter
  end

  test ~s/it doesn't call the configured handler when :skip/ do
    pidload = %{pid: self()}

    {:ok, pid} =
      Projector.start_link(
        deserializer: fn payload -> %Event{payload: pidload} end,
        preprocess: [
          FunctionHandlerSetter,
          SkipReturner
        ]
      )

    assert {:skip, bundle} = GenServer.call(pid, {:consume, %ConsumerMeta{}, nil})

    refute_received FunctionHandlerSetter
  end

  test ~s/it doesn't call the configured handler when :error/ do
    pidload = %{pid: self()}

    {:ok, pid} =
      Projector.start_link(
        deserializer: fn payload -> %Event{payload: pidload} end,
        preprocess: [
          FunctionHandlerSetter,
          ErrorReturner
        ]
      )

    assert {:error, bundle} = GenServer.call(pid, {:consume, %ConsumerMeta{}, nil})

    refute_received FunctionHandlerSetter
  end

  test ~s/it can be configured with postprocessing middleware/ do
    pidload = %{pid: self()}

    {:ok, pid} =
      Projector.start_link(
        deserializer: fn payload -> %Event{payload: pidload} end,
        postprocess: [
          OkReturner
        ]
      )

    assert {:ok, bundle} = GenServer.call(pid, {:consume, %ConsumerMeta{}, nil})

    assert bundle.event.payload == pidload

    assert_receive OkReturner
  end

  test ~s/it can be configured with both preprocessing and postprocessing middleware/ do
    pidload = %{pid: self()}

    {:ok, pid} =
      Projector.start_link(
        deserializer: fn payload -> %Event{payload: pidload} end,
        preprocess: [
          FunctionHandlerSetter,
          OkReturner
        ],
        postprocess: [
          SkipReturner
        ]
      )

    assert {:skip, bundle} = GenServer.call(pid, {:consume, %ConsumerMeta{}, nil})

    assert bundle.event.payload == pidload

    assert_receive OkReturner
    assert_receive FunctionHandlerSetter
    assert_receive SkipReturner
  end

  test ~s/it can retry middleware when requested/ do
    {:ok, agent} = Agent.start(fn -> 0 end)

    pidload = %{pid: self(), counter: agent}

    assert 0 = Agent.get(agent, fn x -> x end)

    {:ok, pid} =
      Projector.start_link(
        deserializer: fn payload -> %Event{payload: pidload} end,
        preprocess: [
          RetryReturner
        ],
        postprocess: [
          RetryReturner
        ]
      )

    # This will retry before eventually returning {:error, _}
    assert {:error, bundle} = Projector.consume(%ConsumerMeta{}, nil)

    assert_received RetryReturner

    assert Agent.get(agent, fn x -> x end) > 1
  end
end
