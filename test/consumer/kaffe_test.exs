defmodule Eidetic.Projector.Consumer.KaffeTest do
  use ExUnit.Case, async: true
  alias Eidetic.Event
  alias Eidetic.Projector
  alias Eidetic.Projector.Bundle
  alias Eidetic.Projector.Bundle.ConsumerMeta
  alias Eidetic.Projector.Consumer.Kaffe, as: KaffeConsumer

  @payload %{
    attributes: 0,
    crc: 1_914_336_469,
    key: "kafka message key",
    magic_byte: 0,
    offset: 41,
    partition: 17,
    topic: "some-kafka-topic",
    value: "the actual kafka message value is here",
    ts: 1_234_567_890_123,
    ts_type: :append
  }

  test ~s/it can proxy Kaffe events to the Projector/ do
    true = Process.register(self(), Projector)

    spawn_link(fn ->
      KaffeConsumer.handle_message(@payload)
    end)

    assert_receive {:"$gen_call", {_pid, _ref}, {:consume, _, _}}
  end

  test ~s/it can convert a Kaffe event into a Projector Event/ do
    true = Process.register(self(), Projector)

    spawn_link(fn ->
      KaffeConsumer.handle_message(@payload)
    end)

    event = @payload.value
    topic = @payload.topic
    partition = @payload.partition
    offset = @payload.offset

    assert_receive {:"$gen_call", {_pid, _ref},
                    {:consume, %ConsumerMeta{topic: topic, partition: partition, offset: offset},
                     event}}
  end

  test ~s/it can convert Projector result into Kaffe expected response/ do
    {:ok, _pid} = Projector.start_link(deserializer: fn payload -> %Event{} end)

    assert :ok = KaffeConsumer.handle_message(@payload)
  end
end
