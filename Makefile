TESTS=

.PHONY: test deps

init:
	@mix local.hex --force
	@mix local.rebar --force

lint:
	@mix format --check-formatted

format:
	@mix format --check-equivalent

deps:
	@mix deps.get

compile:
	@mix compile

test:
	@mix coveralls.html

analyse:
	@mix dialyzer

clean:
	@mix clean --deps

# Work within Docker
dshell:
	@docker-compose run --rm --entrypoint=bash elixir

dclean:
	@docker-compose run --rm --entrypoint=mix elixir clean --deps
	@docker-compose down -v
