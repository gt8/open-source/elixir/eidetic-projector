defmodule Eidetic.Projector.Bundle do
  @moduledoc """
  The data structure used to encapsulate the consumer meta information, event,
  modifiers, and result of our middleware and event handler.
  """

  alias Eidetic.Event
  alias Eidetic.Projector.Bundle.ConsumerMeta

  @type t :: %{
          consumer_meta: %ConsumerMeta{},
          event: %Event{} | nil,
          modifiers: map(),
          handler: atom() | fun() | nil,
          result: {:ok | :error, any()}
        }
  defstruct consumer_meta: %ConsumerMeta{},
            event: nil,
            modifiers: %{},
            handler: nil,
            result: {:ok, nil}
end
