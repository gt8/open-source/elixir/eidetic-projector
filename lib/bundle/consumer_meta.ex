defmodule Eidetic.Projector.Bundle.ConsumerMeta do
  @moduledoc """
  ConsumerMeta contains some metadata about where the event was consumed from
  """
  @type t :: %{
          topic: binary() | nil,
          partition: non_neg_integer() | nil,
          offset: non_neg_integer() | nil
        }
  defstruct topic: nil, partition: nil, offset: nil
end
