defmodule Eidetic.Projector.Deserializer do
  alias Eidetic.Event

  @callback deserializer(any()) :: %Event{}
end
