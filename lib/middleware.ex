defmodule Eidetic.Projector.Middleware do
  alias Eidetic.Projector.Bundle

  @callback process(:pre | :post, %Bundle{}) :: {:ok | :skip | :error, %Bundle{}} | :retry
end
