defmodule Eidetic.Projector.MixProject do
  use Mix.Project

  def project do
    [
      app: :eidetic_projector,
      version: "0.1.0-alpha7",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env()),
      test_coverage: [tool: ExCoveralls],
      dialyzer_ignored_warnings: [
        # Throws errors for Elixir source with these omitted
        {:warn_contract_supertype, :_, :_}
      ],
      description: description(),
      package: package()
    ]
  end

  defp description() do
    """
    Projector Middleware system for building projections from Eidetic event streams
    """
  end

  defp package do
    [
      name: :eidetic_projector,
      files: ["lib", "mix.exs", "README.md", "LICENSE"],
      maintainers: ["GT8 Online"],
      licenses: ["MIT"],
      links: %{"Source Code" => "https://gitlab.com/gt8/open-source/elixir/eidetic-projector"}
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp elixirc_paths(:dev), do: ["lib"]
  defp elixirc_paths(:test), do: ["test"] ++ elixirc_paths(:dev)
  defp elixirc_paths(_), do: ["lib"]

  defp deps do
    [
      {:eidetic, "~> 1.0.0-alpha2"},
      {:mongodb, "~> 0.4.6"},
      {:retry, "~> 0.10.0"},
      {:dialyzex, "~> 1.1.0", only: :test},
      {:ex_doc, "~> 0.19", only: :dev, runtime: false},
      {:excoveralls, "~> 0.7", only: :test}
    ]
  end
end
